EESchema Schematic File Version 4
LIBS:_saved_raspberrypi_zerow_uhat-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title "Input Signal Module"
Date "2021-03-06"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7450 6950 0    79   ~ 16
EEE3088F Project Hand-in 3\nItumeleng Seeco
Wire Wire Line
	5500 3550 5600 3550
Text HLabel 5600 2800 0    50   Input ~ 0
GPIO6
$Comp
L Device:R R15
U 1 1 60BA57F7
P 5200 4000
F 0 "R15" V 4993 4000 50  0000 C CNN
F 1 "10k" V 5084 4000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5130 4000 50  0001 C CNN
F 3 "~" H 5200 4000 50  0001 C CNN
	1    5200 4000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R14
U 1 1 60BA629C
P 4900 4350
F 0 "R14" V 4693 4350 50  0000 C CNN
F 1 "33k" V 4784 4350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4830 4350 50  0001 C CNN
F 3 "~" H 4900 4350 50  0001 C CNN
	1    4900 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 4200 4900 4000
Wire Wire Line
	5050 4000 4900 4000
Connection ~ 4900 4000
Wire Wire Line
	4900 4000 4900 3650
Wire Wire Line
	5350 4000 5600 4000
Wire Wire Line
	5600 4000 5600 3550
Connection ~ 5600 3550
Wire Wire Line
	5600 3550 5600 2800
Wire Wire Line
	4900 4500 4900 4750
Text GLabel 4900 4750 0    50   Input ~ 0
GND
$Comp
L Device:R R16
U 1 1 60BBB095
P 5500 5500
F 0 "R16" V 5293 5500 50  0000 C CNN
F 1 "10k" V 5384 5500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5430 5500 50  0001 C CNN
F 3 "~" H 5500 5500 50  0001 C CNN
	1    5500 5500
	-1   0    0    1   
$EndComp
$Comp
L Device:R R17
U 1 1 60BBB09B
P 5500 6400
F 0 "R17" V 5293 6400 50  0000 C CNN
F 1 "10k" V 5384 6400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5430 6400 50  0001 C CNN
F 3 "~" H 5500 6400 50  0001 C CNN
	1    5500 6400
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 5850 6700 5850
Wire Wire Line
	5500 6250 5500 5750
Wire Wire Line
	5500 5750 6000 5750
Connection ~ 5500 5750
Wire Wire Line
	5500 5750 5500 5650
Wire Wire Line
	5500 5350 5500 4950
Wire Wire Line
	5500 6800 5500 6550
Text GLabel 5500 6800 0    50   Input ~ 0
GND
Text HLabel 5500 4950 0    50   Input ~ 0
HEADER_P3
$Comp
L Device:R R19
U 1 1 60BBB0B3
P 6300 6300
F 0 "R19" V 6093 6300 50  0000 C CNN
F 1 "10k" V 6184 6300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 6230 6300 50  0001 C CNN
F 3 "~" H 6300 6300 50  0001 C CNN
	1    6300 6300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R18
U 1 1 60BBB0B9
P 6000 6650
F 0 "R18" V 5793 6650 50  0000 C CNN
F 1 "33k" V 5884 6650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5930 6650 50  0001 C CNN
F 3 "~" H 6000 6650 50  0001 C CNN
	1    6000 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 6500 6000 6300
Wire Wire Line
	6150 6300 6000 6300
Connection ~ 6000 6300
Wire Wire Line
	6000 6300 6000 5950
Wire Wire Line
	6450 6300 6700 6300
Wire Wire Line
	6700 6300 6700 5850
Wire Wire Line
	6000 6800 6000 7050
Text GLabel 6000 7050 0    50   Input ~ 0
GND
$Comp
L Device:R R8
U 1 1 60BC5F9D
P 3400 1150
F 0 "R8" V 3193 1150 50  0000 C CNN
F 1 "10k" V 3284 1150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3330 1150 50  0001 C CNN
F 3 "~" H 3400 1150 50  0001 C CNN
	1    3400 1150
	-1   0    0    1   
$EndComp
$Comp
L Device:R R9
U 1 1 60BC5FA3
P 3400 2050
F 0 "R9" V 3193 2050 50  0000 C CNN
F 1 "10k" V 3284 2050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3330 2050 50  0001 C CNN
F 3 "~" H 3400 2050 50  0001 C CNN
	1    3400 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 1900 3400 1400
Wire Wire Line
	3400 1400 3900 1400
Connection ~ 3400 1400
Wire Wire Line
	3400 1400 3400 1300
Wire Wire Line
	3400 1000 3400 600 
Wire Wire Line
	3400 2450 3400 2200
Text GLabel 3400 2450 0    50   Input ~ 0
GND
Text HLabel 4600 950  0    50   Input ~ 0
GPIO5
$Comp
L Device:R R10
U 1 1 60BC5FC1
P 3900 2300
F 0 "R10" V 3693 2300 50  0000 C CNN
F 1 "33k" V 3784 2300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3830 2300 50  0001 C CNN
F 3 "~" H 3900 2300 50  0001 C CNN
	1    3900 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2150 3900 1950
Wire Wire Line
	4050 1950 3900 1950
Connection ~ 3900 1950
Wire Wire Line
	3900 1950 3900 1600
Wire Wire Line
	3900 2450 3900 2700
Text GLabel 3900 2700 0    50   Input ~ 0
GND
Text HLabel 3400 600  0    50   Input ~ 0
HEADER_P1
Text HLabel 4400 2650 0    50   Input ~ 0
HEADER_P2
Wire Wire Line
	4400 3050 4400 2650
Wire Wire Line
	4400 3450 4900 3450
Connection ~ 4400 3450
Wire Wire Line
	4400 3450 4400 3350
$Comp
L Device:R R12
U 1 1 606BAD55
P 4400 3200
F 0 "R12" V 4193 3200 50  0000 C CNN
F 1 "10k" V 4284 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4330 3200 50  0001 C CNN
F 3 "~" H 4400 3200 50  0001 C CNN
	1    4400 3200
	-1   0    0    1   
$EndComp
Text GLabel 4400 4500 0    50   Input ~ 0
GND
Wire Wire Line
	4400 4500 4400 4250
Wire Wire Line
	4400 3950 4400 3450
$Comp
L Device:R R13
U 1 1 606BB335
P 4400 4100
F 0 "R13" V 4193 4100 50  0000 C CNN
F 1 "10k" V 4284 4100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4330 4100 50  0001 C CNN
F 3 "~" H 4400 4100 50  0001 C CNN
	1    4400 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4350 1950 4600 1950
$Comp
L Device:R R11
U 1 1 60BC5FBB
P 4200 1950
F 0 "R11" V 3993 1950 50  0000 C CNN
F 1 "10k" V 4084 1950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4130 1950 50  0001 C CNN
F 3 "~" H 4200 1950 50  0001 C CNN
	1    4200 1950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4600 1950 4600 1500
Wire Wire Line
	4600 1500 4600 950 
Connection ~ 4600 1500
Wire Wire Line
	4500 1500 4600 1500
$Comp
L Amplifier_Operational:LM324 U1
U 1 1 60F52520
P 4200 1500
F 0 "U1" H 4200 1867 50  0000 C CNN
F 1 "LM324" H 4200 1776 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W10.16mm" H 4150 1600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 4250 1700 50  0001 C CNN
	1    4200 1500
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324 U1
U 2 1 60F5494C
P 6300 5850
F 0 "U1" H 6300 6217 50  0000 C CNN
F 1 "LM324" H 6300 6126 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W10.16mm" H 6250 5950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 6350 6050 50  0001 C CNN
	2    6300 5850
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324 U1
U 3 1 60F55317
P 5200 3550
F 0 "U1" H 5200 3917 50  0000 C CNN
F 1 "LM324" H 5200 3826 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W10.16mm" H 5150 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 5250 3750 50  0001 C CNN
	3    5200 3550
	1    0    0    -1  
$EndComp
Text HLabel 6700 5100 0    50   Input ~ 0
GPIO13
Wire Wire Line
	6700 5850 6700 5100
$EndSCHEMATC
