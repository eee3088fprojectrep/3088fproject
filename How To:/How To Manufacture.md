# How to manufacture the position feedback uHAT

- Download the following folder and save it on your computer
- Extract all the files and save them into a new folder which you'll name Position_PiHAt
- Open the folder and open the KiCAD pcb file.
- Genereate the Gerber files, drill files, Footprint Position files and a bill of Materials which you can find here.... and save these in a folder called Outputs
- Compress the Outputs folder into an Outputs zip files.
- Your PCB is now ready to be sent to the manufacturers, simply send them the Outputs zip file.
