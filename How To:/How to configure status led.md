# How to : Configure the status LEDs to the Rasberry PI;

## Code snippet(Python):
### Replace the variables pin1 and pin2 names the associated pins displayed on the Raspberry Pi board and Pi hat pin.
 `1 import RPi.GPIO as GPIO`  
 `2 GPIO.setup(pin, GPIO.IN) // set pin as input pin(data coming in from sensors  `  
 `3 GPIO.setup(pin2, GPIO.OUT)  //set pin as output pin(display data on leds `   
 `4 int i = GPIO.input(pin)  `     
 `5`      
 `6 if GPIO.input(i):  `    
 `7     GPIO.output(pin,GPIO.HIGH)   //Turn on led given data on pin1  `    
 `8     print("LED turned is on, condition met")`    
 `9 else:   `  
`10     GPIO.output(pin,GPIO.LOW)   //Turn off led given data on pin1`   
`11     print("LED is off, condition not met")`

### To configure the rest of the LEDs , repeat lines 2-11, for the individual pin in/out’s of the pin hat  
