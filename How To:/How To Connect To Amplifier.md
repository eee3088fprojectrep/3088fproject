# HOW TO CONNECT TO THE AMPLIFIER CIRCUIT
### The following configuration must be used to connect to the amplifier subsystem:
* []	The output from the Hall Sensors must be connected to the Header_P1, Header_P2 and Header P3, i.e., Pin 4,5 and 6 of the 01 x 08 Female Connector respectively. 
* []	The Vcc of the Hall Effect sensors is connected to the Vcc of the Motor, and this is supplied from Pin 7 of the 01 x 08 Female Connector.
* []	Ground connection is from Pin 8 of the 01 x 08 Female Connector.

### The output from the Operational amplifiers is supplied to GPIO pin 5,6 and 13 of the Raspberry Pi through the 40-pin GPIO connector. The data to these pins may then be used to determine the speed and direction of the motor.
