EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Power Supply for uHat"
Date "2021-06-06"
Rev "02.1"
Comp "Takunda Makoni"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:CP1 C1
U 1 1 60B44347
P 5900 3700
F 0 "C1" H 6015 3746 50  0000 L CNN
F 1 "22u" H 6015 3655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 5900 3700 50  0001 C CNN
F 3 "~" H 5900 3700 50  0001 C CNN
	1    5900 3700
	1    0    0    -1  
$EndComp
$Comp
L pspice:INDUCTOR L1
U 1 1 60B37AE2
P 4400 2700
F 0 "L1" H 4400 2915 50  0000 C CNN
F 1 "470u" H 4400 2824 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L5.3mm_D2.2mm_P2.54mm_Vertical_Vishay_IM-1" H 4400 2700 50  0001 C CNN
F 3 "~" H 4400 2700 50  0001 C CNN
	1    4400 2700
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:BSB012NE2LXI Q1
U 1 1 60B821B0
P 4700 3550
F 0 "Q1" H 4904 3596 50  0000 L CNN
F 1 "BSB012NE2LXI" H 4904 3505 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:Texas_DRT-3" H 4700 3550 50  0001 C CIN
F 3 "https://www.infineon.com/dgdl/Infineon-BSB012NE2LXI-DS-v02_01-EN.pdf?fileId=db3a30433f764301013f805e3eb247c1" H 4700 3550 50  0001 L CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
Connection ~ 3950 2700
Wire Wire Line
	3950 2700 3500 2700
Wire Wire Line
	4150 2700 3950 2700
Wire Wire Line
	3950 3550 3950 2700
Wire Wire Line
	4500 3550 3950 3550
Wire Wire Line
	4650 2700 4800 2700
Wire Wire Line
	4800 3350 4800 2700
Wire Wire Line
	5900 3550 5900 2700
$Comp
L Device:R R20
U 1 1 60BA771E
P 6950 3000
F 0 "R20" H 7020 3046 50  0000 L CNN
F 1 "950" H 7020 2955 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 6880 3000 50  0001 C CNN
F 3 "~" H 6950 3000 50  0001 C CNN
	1    6950 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 60BA7D16
P 6950 4150
F 0 "R21" H 7020 4196 50  0000 L CNN
F 1 "250" H 7020 4105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 6880 4150 50  0001 C CNN
F 3 "~" H 6950 4150 50  0001 C CNN
	1    6950 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3750 4800 4450
Wire Wire Line
	5900 3850 5900 4450
Wire Wire Line
	4800 4450 3500 4450
Connection ~ 4800 4450
Wire Wire Line
	3500 2800 3500 4450
Wire Wire Line
	6950 3300 7450 3300
Connection ~ 5900 4450
$Comp
L Diode:MBR0540 D8
U 1 1 60BAD4CD
P 5350 2700
F 0 "D8" H 5350 2483 50  0000 C CNN
F 1 "MBR0540" H 5350 2574 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P5.08mm_Vertical_KathodeUp" H 5350 2525 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/MBR0520~MBR0580(SOD123).pdf" H 5350 2700 50  0001 C CNN
	1    5350 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	5500 2700 5900 2700
Connection ~ 5900 2700
Wire Wire Line
	5200 2700 4800 2700
Connection ~ 4800 2700
Wire Wire Line
	4800 4450 5900 4450
Text GLabel 5250 4450 3    50   Input ~ 0
GND
Text HLabel 7450 2700 2    50   Input ~ 0
24V
Text HLabel 7450 3900 2    50   Input ~ 0
5V
Text HLabel 3500 2800 0    50   Input ~ 0
GND
Text HLabel 3500 2700 0    50   Input ~ 0
Vin
Wire Wire Line
	5900 2700 6950 2700
$Comp
L Device:R R28
U 1 1 60C2E06F
P 6950 3600
F 0 "R28" H 7020 3646 50  0000 L CNN
F 1 "175" H 7020 3555 50  0000 L CNN
F 2 "" V 6880 3600 50  0001 C CNN
F 3 "~" H 6950 3600 50  0001 C CNN
	1    6950 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2700 6950 2850
Wire Wire Line
	6950 3150 6950 3300
Wire Wire Line
	6950 4450 6950 4300
Wire Wire Line
	5900 4450 6950 4450
Connection ~ 6950 3300
Wire Wire Line
	6950 3300 6950 3450
Text HLabel 7450 3300 2    50   Input ~ 0
8.5V
Wire Wire Line
	6950 3750 6950 3900
Wire Wire Line
	6950 2700 7450 2700
Connection ~ 6950 2700
Wire Wire Line
	6950 3900 7450 3900
Connection ~ 6950 3900
Wire Wire Line
	6950 3900 6950 4000
$EndSCHEMATC
